<div class="row"> 
        	<br>
			<h4>Rekapitulacija</h4>
		</div>

		<div class="row"> 
			<div class="col-7"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>Osnovica</td>
							<td>PDV</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>Oslobođeno poreza</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po posebnoj stopi</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po opštoj stopi</td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
						<tr>
							<td><strong>Ukupno</strong></td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		        		 @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
		                 {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                 @else
		                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
		                 @endif
		            </tr>
		            <tr class="text-right">
		                @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                @else
		                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>0,00</td>
		                @endif
		            </tr>
					@if(($cena_dostave = AdminCommon::cena_dostave($web_b2c_narudzbina_id)) > 0)
		            <tr class="text-right">		                
		                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
		                	{{AdminCommon::cena($cena_dostave)}}                	
		             	</td>
		            </tr>
		            @endif
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                {{AdminCommon::cena((AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::cena_dostave($web_b2c_narudzbina_id)))}}</td>
		            </tr>
		        </table>
	        </div>
		</div>
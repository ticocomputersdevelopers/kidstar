<?php
namespace Import;
use Import\Support;
use DB;
use File; 
use PHPExcel; 
use PHPExcel_IOFactory;

class Gembird {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
		if($extension==null){
			$username=Support::serviceUsername($dobavljac_id);
			$password=Support::servicePassword($dobavljac_id);

			self::autoDownload($username,$password,'files/gembird/gembird_xml/gembird.xml');
			$products_file = "files/gembird/gembird_xml/gembird.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			$products = simplexml_load_file($products_file);
			// $handle = fopen($products, "r");
			// var_dump($handle);die;

			foreach ($products as $product):
				if(!empty($product->id)){
					$opis = $product->xpath('specifications/attribute_group/attribute/value')[0];
					$cena = floatval(str_replace(',','',$product->price))/1.2;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->name)) . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->category)) . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->manufacturer)) . "',";
					$sPolja .= "opis,";						$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($opis)) . "',";
					$sPolja .= "pdv,";						$sVrednosti .= "" . $product->vat . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . ($product->stock=='Ima'?"1":"0") . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					$images = $product->xpath('images/image');
					for($i=0;$i<count($images);$i++){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->id).",'".str_replace("https", "http", $images[$i])."',".($i==0?"1":"0").")");
					}

				}

			endforeach;
			
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i'));
	
				$file_name = Support::file_name("files/gembird/gembird_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/gembird/gembird_xml/".$file_name);
		}
		}
}


	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
				
		if($extension==null){
			$username=Support::serviceUsername($dobavljac_id);
			$password=Support::servicePassword($dobavljac_id);

			self::autoDownload($username,$password,'files/gembird/gembird_xml/gembird.xml');
			$products_file = "files/gembird/gembird_xml/gembird.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			$products = simplexml_load_file($products_file);


			foreach ($products as $product):

			if(!empty($product->id)){
					$opis = $product->xpath('specifications/attribute_group/attribute/value')[0];
					$cena = floatval(str_replace(',','',$product->price))/1.2;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . ($product->stock=='Ima'?"1":"0") . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
			}
			endforeach;		
	
		$file_name = Support::file_name("files/gembird/gembird_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/gembird/gembird_xml/".$file_name);
		}
	}
}
	public static function autoDownload($username,$password,$file){
		
		$userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
		$cookieFile = 'cookie.txt';
		$loginFormUrl = 'https://www.gembird.rs/b2b/login';
		$loginActionUrl = 'https://www.gembird.rs/b2b/login/store';
		 
		$postValues = array(
		    'username' => $username,
		    'password' => $password
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_exec($curl);
		if(curl_errno($curl)){
		    throw new Exception(curl_error($curl));
		}
		curl_setopt($curl, CURLOPT_URL, 'https://www.gembird.rs/b2b/xml-export');
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
		curl_exec($curl);
		curl_close($curl);
	}
}
<?php
use Export\BackupArticles;

class AdminBackupController extends Controller {

	function backup($backup_sifra,$kind,$key){

		if($backup_sifra == 'articles'){
			BackupArticles::execute($kind);
			AdminSupport::saveLog('ARTIKLI_BACKUP');
		}elseif($backup_sifra == 'images'){
			if($kind=='zip'){
				AdminSupport::zip_images();
				AdminSupport::saveLog('ARTIKLI_SLIKE_BACKUP');
			}else{
				return Redirect::to(AdminOptions::base_url());
			}
		}else{
			return Redirect::to(AdminOptions::base_url());
		}
	}


}
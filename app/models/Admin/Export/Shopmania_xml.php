<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;

class Shopmania_xml {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis,grupa_pr_id, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, akcijska_cena, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}
	public static function puna_putanja_grupe($grupa_pr_id,$link=''){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
        if($link!=''){
        	$link = $first->grupa.'-'.$link;
        }else{
        	$link = $first->grupa;
        }
        if($first->parrent_grupa_pr_id != 0){
        	$link = self::puna_putanja_grupe($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("store");
		$xml->appendChild($root);

		foreach($products as $article){

			$product   = $xml->createElement("product");

		    Support::xml_node($xml,"MPC",$article->roba_id,$product);
		    Support::xml_node($xml,"Category",self::puna_putanja_grupe($article->grupa_pr_id),$product);
		    Support::xml_node($xml,"Manufacturer",$article->proizvodjac,$product);
		    Support::xml_node($xml,"Model",$article->model,$product);
		    Support::xml_node($xml,"Name",$article->naziv_web,$product);
			// Support::xml_node($xml,"Description",strip_tags($article->web_opis)."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,strip_tags($article->web_karakteristike)),$product);
		    Support::xml_node($xml,"URL",Support::product_link($article->roba_id),$product);
		    Support::xml_node($xml,"Image",Support::major_image($article->roba_id),$product);
		    Support::xml_node($xml,"Price",($article->akcija_flag_primeni==1?$article->akcijska_cena:$article->web_cena),$product);
		    Support::xml_node($xml,"Currency",'RSD',$product);
			// Support::xml_node($xml,"troskovi_isporuke",'besplatna isporuka',$product);
		    Support::xml_node($xml,"Availability",(self::lager($article->roba_id) > 0 ? 'In stock / Na zalihama':'Out of stock / Nema na zalihama'),$product);
		    Support::xml_node($xml,"GTIN",$article->barkod,$product);

			// Support::xml_node($xml,"troskovi_isporuke",Support::troskovi_isporuke($article->tezinski_faktor),$product);

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/shopmania/shopmania_xml.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}
	public static function lager($roba_id){
		return DB::table('lager')->where('roba_id',$roba_id)->pluck('kolicina');;
	}

}
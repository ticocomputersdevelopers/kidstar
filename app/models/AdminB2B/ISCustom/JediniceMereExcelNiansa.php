<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;

class JediniceMereExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/jedinica_mere.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(3);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array("(-1,'Nedefinisana',NULL,(NULL)::integer,1,'NEDEF',0)","(0,'Nedefinisana',NULL,(NULL)::integer,1,NULL,0)");
		$manufacturer_ids = array(-1,0);
		
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $B = $worksheet->getCell('B'.$row)->getValue();

		    if(is_numeric($A)){
				$jedinica_mere_ids[] = intval($A);
				$result_arr[] = "(".intval($A).",'".$B."',NULL,(NULL)::integer,1,NULL,0)";
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"ids"=>$jedinica_mere_ids);
	}


}
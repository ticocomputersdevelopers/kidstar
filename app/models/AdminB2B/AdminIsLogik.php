<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsLogik\DBData;

use IsLogik\Support;
use IsLogik\Group;
use IsLogik\ArticleGroup;
use IsLogik\Article;
use IsLogik\Stock;
use IsLogik\Partner;
use IsLogik\PartnerGroup;
use IsLogik\PartnerCard;


class AdminIsLogik {

    public static function execute(){
        // if(AdminB2BIS::checkSynchronization()){
        //     return (object) array('success'=>false,'message'=>'Sinhronizacija je već pokrenuta!');
        // }
        // AdminB2BIS::synchronization(true);

        // try {
            //groups
            $groups = DBData::groups();
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body,array('parrent_grupa_pr_id'));
            // Group::query_update_unexists($resultGroup->body);
            Support::updateGroupsParent($groups);
            DBData::update_groups();
            $mapped_groups = Support::getMappedGroups();

            //articles
            $articles = DBData::articles();

            $resultArticle = Article::table_body($articles,$mapped_groups);
            Article::query_insert_update($resultArticle->body,array('sifra_is','naziv','naziv_web','web_opis','flag_aktivan','grupa_pr_id','jedinica_mere_id','tarifna_grupa_id','barkod','model','racunska_cena_nc','racunska_cena_a','mpcena','akcija_flag_primeni','b2b_max_rabat','b2b_akcijski_rabat'));
            DBData::update_articles($resultArticle->ids_is);
            $mapped_articles = Support::getMappedArticles();
            //articles_groups
            $articles_groups = DBData::articles_groups();
            $resultArticleGroup = ArticleGroup::table_body($articles_groups,$mapped_articles,$mapped_groups);
            ArticleGroup::query_insert_delete($resultArticleGroup->body);
            DBData::update_articles_groups($mapped_articles);

            //stocks
            $resultStock = Stock::table_body($articles,$mapped_articles);
            Stock::query_insert_update($resultStock->body);

            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('naziv','adresa','mesto','telefon','fax','rabat','limit_p','dani_valute','naziv_puni','login','password','mail','komercijalista'));
            DBData::update_partners($resultPartner->ids_is);
            $mapped_partners = Support::getMappedPartners();

            //partner group
            $partnersGroups = DBData::partners_groups_rabat();
            $resultPartnersGroups = PartnerGroup::table_body($partnersGroups,$mapped_groups,$mapped_partners);
            PartnerGroup::query_insert_update($resultPartnersGroups->body);
            DBData::update_partners_groups($resultPartnersGroups->mapped_result);

            //partner card
            $partnersCards = DBData::partners_cards();
            $resultPartnersCards = PartnerCard::table_body($partnersCards,$mapped_partners);
            PartnerCard::query_insert_update($resultPartnersCards->body);
            DBData::update_partners_cards();

        //     AdminB2BIS::synchronization(false);
        //     AdminB2BIS::saveISLog('true');
        //     return (object) array('success'=>true);
        // }catch (Exception $e){
        //     AdminB2BIS::synchronization(false);
        //     AdminB2BIS::saveISLog('false');
        //     AdminB2BIS::sendNotification(array(9,12,15,18),10,0);
        //     return (object) array('success'=>false,'message'=>$e->getMessage());
        // }
    }


}
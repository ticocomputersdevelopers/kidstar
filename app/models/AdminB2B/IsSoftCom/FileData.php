<?php
namespace IsSoftCom;

use DB;
use File;
use All;


class FileData {

	public static function articles(){
		$file_path = "files/IS/dat/roba.dat";

		if(!File::exists($file_path)){
			return array();
		}

		$subject = file_get_contents($file_path);
		$subject = preg_replace("/\n(^(?![0-9]{1,4}|))/im","$1",$subject);
		$rows = explode("\n",$subject);

		$file_products = array_filter($rows,function($row){
			return count(explode('|',$row)) == 10;
		});

		$file_products = array_map(function($row){
			$product = explode('|',$row);
			preg_match('/(^[0-9]{1,4}) (.*)/im',$product[3],$groupMatches);

			return (object) array(
				'id' => $product[0],
				'naziv' => $product[1],
				'jedinica_mere' => $product[2],
				'grupaid' => isset($groupMatches[1]) && !empty($groupMatches[1]) ? $groupMatches[1] : '',
				'grupa' => isset($groupMatches[2]) && !empty($groupMatches[2]) ? $groupMatches[2] : '000',
				'podgrupaid' => $product[4],
				'pdv' => intval($product[5]),
				'opis' => $product[6],
				'paket_kolicina' => intval(str_replace(',','.',$product[7])),
				'cena' => number_format(round(floatval(str_replace(',','.',$product[8])),2),2,".",""),
				'kolicina' => intval(str_replace(',','.',$product[9]))
			);
		},$file_products);

		// File::delete($file_path);
		return $file_products;
	}

	// public static function partners(){
	// 	$file_path = "files/IS/dat/partner.dat";

	// 	$partners = array();
	// 	if(!File::exists($file_path)){
	// 		return $partners;
	// 	}

	// 	$xml_partners = simplexml_load_file($file_path);
	// 	File::delete($file_path);
	// 	return $xml_partners;
	// }


}
<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminArticles;
use AdminCommon;
use DOMDocument;
use Response;
use SimpleXMLElement;
use View;

class Facebook {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike,grupa_pr_id,mpcena,web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, akcijska_cena, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
			$xml = new SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0" />'); // May as well chuck the google ns in the root element declaration here, while we're at it, rather than adding it via a separate attribute.
			$xml->addAttribute('version', '2.0'); 
			// $xml->addAttribute('hack:xmlns:g','http://base.google.com/ns/1.0'); //Or could do this instead...

			$xml->addChild('channel');
			$xml->channel->addChild('title', 'HAOS (kidstardoo)');
			$xml->channel->addChild('description', 'Facebook Product List RSS feed');
			$xml->channel->addChild('link', Support::base_url());

		foreach($products as $product){

		    $item = $xml->channel->addChild('item');

		    $item->addChild('hack:g:id', $product->roba_id);
		    // $item->addChild('hack:g:item_group_id', $product->grupa_pr_id);
		    $item->addChild('hack:g:title', htmlspecialchars(ltrim($product->naziv_web)));
		    $item->addChild('hack:g:product_type', htmlspecialchars($product->grupa));
		    $item->addChild('hack:g:description',htmlspecialchars(str_replace(array("c&#769;","&#8211;","&#225;","&#283;","&#345;","&#253;","&#237;","&#233;","&#367;","&#328;","&#250;","&#956;","&scaron;"),array("ć","-","á","ě","ř","ý","í","é","ů","ň","ú","μ","š"), $product->web_opis))) ;
			$item->addChild('hack:g:link', AdminArticles::article_link(htmlspecialchars($product->roba_id)));
			$item->addChild('hack:g:image_link', Support::major_image($product->roba_id));
			foreach (self::slike($product->roba_id) as $image ) {			    	
			   	$item->addChild('hack:g:additional_image_link',AdminOptions::base_url().$image);
			}
			$item->addChild('hack:g:price', $product->web_cena);
			$item->addChild('hack:g:sale_price', $product->akcija_flag_primeni==1?$product->akcijska_cena:'');
			$item->addChild('hack:g:availability', $product->kolicina > 0 ? 'in stock' : 'out of stock');
			$item->addChild('hack:g:google_product_category', self::group_link($product->grupa_pr_id));
			$item->addChild('hack:g:mpn', $product->mpcena);
			$item->addChild('hack:g:condition', 'new');
		}

		
		$store_path = 'files/exporti/facebook/facebook-feed.xml';
		$xml->asXML($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}
    public static function group_link($group_id,$lang=null)
	{	$slug = null;
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where(array('grupa_pr_id'=>$group_id,'web_b2c_prikazi'=>1))->first();
		if(!is_null($grupa)){
			$slug = AdminOptions::slug_trans($grupa->grupa,$lang);
			if($grupa->parrent_grupa_pr_id > 0 && $grupa->grupa_pr_id != $grupa->parrent_grupa_pr_id){
				$subSlug = self::group_link($grupa->parrent_grupa_pr_id,$lang);
				if(is_null($subSlug)){
					return null;
				}
				$slug = $subSlug.' > '.$slug;
			}
		}
		return $slug;
	}
	public static function slike($roba_id){
        return array_map('current',DB::table('web_slika')->select('putanja')->where('akcija',0)->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get());	
	}
}
<?php
namespace Import;
use Import\Support;
use DB;
use File;

class OFY { 

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/ofy/ofy_xml/ofy.xml');
			$products_file = "files/ofy/ofy_xml/ofy.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	

			foreach ($products as $product):
				if(!empty($product->sifra_artikla)){
					
					$cena = floatval(str_replace(',','',$product->mpcena));
					$naziv = Support::convertChars(Support::encodeTo1250($product->naziv)) !== NULL ? Support::convertChars(Support::encodeTo1250($product->naziv)) : "";
					$grupa = addslashes(Support::zameni_karaktere_import(Support::encodeTo1250($product->grupa))) !== NULL ? addslashes(Support::zameni_karaktere_import(Support::encodeTo1250($product->grupa))) : "";
					$proizvodjac = addslashes(Support::encodeTo1250($product->proizvodjac)) !== NULL ? addslashes(Support::encodeTo1250($product->proizvodjac)) : "";
					$opis = Support::convertChars(addslashes(Support::encodeTo1250($product->opis))) !== NULL ? Support::convertChars(addslashes(Support::encodeTo1250($product->opis))) : "";
					$pdv = $product->pdv > 1 ? $product->pdv : 0;
					$kolicina = $product->kolicina !== NULL ? $product->kolicina : 0;
					$mpcena = number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') !== NULL ? number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') : 0;
					$sifra_kod_dobavljaca = addslashes(Support::encodeTo1250($product->sifra_artikla)) !== NULL ? addslashes(Support::encodeTo1250($product->sifra_artikla)) : "";

					$naziv= pg_escape_string($naziv);
					$opis = pg_escape_string($opis);

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra_kod_dobavljaca . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . $naziv . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . $grupa . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
					$sPolja .= "opis,";						$sVrednosti .= "'" . $opis . "',";
					$sPolja .= "pdv,";						$sVrednosti .= "" . $pdv . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
					$sPolja .= "mpcena,";					$sVrednosti .= "" . $mpcena . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . $mpcena . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					$images = $product->xpath('slike/slika');
					for($i=0;$i<count($images);$i++){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->sifra_artikla).",'".substr($images[$i],3,-2)."',".($i==0?"1":"0").")");
					}

				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/ofy/ofy_xml/ofy.xml');
			$products_file = "files/ofy/ofy_xml/ofy.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){
					$cena = floatval(str_replace(',','',$product->price))/1.2;
					$sifra_kod_dobavljaca = addslashes(Support::encodeTo1250($product->sifra_artikla)) !== NULL ? addslashes(Support::encodeTo1250($product->sifra_artikla)) : "";
					$kolicina = $product->kolicina !== NULL ? $product->kolicina : 0;
					$mpcena = number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') !== NULL ? number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') : 0;
					
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra_kod_dobavljaca . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
					$sPolja .= "mpcena,";					$sVrednosti .= "" . $mpcena . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . $mpcena . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}
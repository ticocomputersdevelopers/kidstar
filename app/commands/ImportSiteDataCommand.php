<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class ImportSiteDataCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:site:data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function convert($string){
		// $string = pg_escape_string($string);
		// $string = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		// return $string;

		$string = str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$string);
		
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        // $string = str_replace(array("\n","&#352;","&#353;","&#268;","&#269;","&#272;","&#273;","&#381;","&#382;","&#262;","&#263;"),array("<br>","Š","š","Č","č","Đ","đ","Ž","ž","Ć","ć"),$string);
		$string = str_replace(array("\n"),array("<br>"),$string);

        return $string;
	}

	public static function fileExtension($image_path){
		$extension = 'jpg';
    	$slash_parts = explode('/',$image_path);
    	$old_name = $slash_parts[count($slash_parts)-1];
    	$old_name_arr = explode('.',$old_name);
    	$extension = $old_name_arr[count($old_name_arr)-1];
    	return $extension;
	}

	public static function uniqueGroups($products){
		$groups = [];
		foreach($products as $product){
			$groups[] = trim($product->grupa);
		}
		return array_unique($groups);
	}

	public static function features($products){
		$features = [];
		foreach ($products as $product) {
			if(!isset($features[trim(strval($product->grupa))])){
				$features[trim(strval($product->grupa))] = [];
			}

			foreach($product->xpath('karakteristike/karakteristika') as $karakteristika){
				if(!empty($naziv = $karakteristika->attributes()->ime) && !empty($vrednost = trim(strval($karakteristika->vrednost)))){
					$vrednost = substr($vrednost,0,255);

					if(!isset($features[trim(strval($product->grupa))][trim($naziv)])){
						$features[trim(strval($product->grupa))][trim($naziv)] = [];
					}

					if(!in_array($vrednost,$features[trim(strval($product->grupa))][trim($naziv)])){
						$features[trim(strval($product->grupa))][trim($naziv)][] = $vrednost;
					}


				}
			}
		}
		return $features;
	}


	public static function uniqueManufacturers($products){
		$manufacturers = [];
		foreach($products as $product){
			$manufacturers[] = trim($product->proizvodjac);
		}
		return array_unique($manufacturers);
	}

	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}

	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $manufacturer){
			$mapped[$manufacturer->id_is] = $manufacturer->proizvodjac_id;
		}
		return $mapped;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->id_is)] = $article->roba_id;
		}
		return $mapped;
	}

	public static function getMappedFeatureNames(){
		$mapped = array();
		$features = DB::table('grupa_pr_naziv')->join('grupa_pr','grupa_pr.grupa_pr_id','=','grupa_pr_naziv.grupa_pr_id')->select('grupa_pr_naziv_id','grupa','naziv')->get();
		foreach($features as $feature){
			$mapped[trim($feature->grupa).'<=>'.trim($feature->naziv)] = $feature->grupa_pr_naziv_id;
		}
		return $mapped;
	}

	public static function getMappedFeatureValues(){
		$mapped = array();
		$features = DB::table('grupa_pr_vrednost')->join('grupa_pr_naziv','grupa_pr_naziv.grupa_pr_naziv_id','=','grupa_pr_vrednost.grupa_pr_naziv_id')->join('grupa_pr','grupa_pr.grupa_pr_id','=','grupa_pr_naziv.grupa_pr_id')->select('grupa_pr_vrednost_id','grupa','grupa_pr_naziv.naziv as naziv','grupa_pr_vrednost.naziv as vrednost')->get();
		foreach($features as $feature){
			$mapped[trim($feature->grupa).'<=>'.trim($feature->naziv).'<=>'.trim($feature->vrednost)] = $feature->grupa_pr_vrednost_id;
		}
		return $mapped;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$products = simplexml_load_file("product_import.xml");

		$group_rows ="";
		foreach (self::uniqueGroups($products) as $key => $groupName) {
			$group_rows .= "(nextval('grupa_pr_grupa_pr_id_seq'),'".$groupName."',NULL,0,nextval('grupa_pr_grupa_pr_id_seq'),1,1,1,0,NULL,NULL,NULL,NULL,0,".strval($key+1).",NULL,NULL,(NULL)::integer,NULL,'".$groupName."',NULL),";
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$table_temp = "(VALUES ".substr($group_rows,0,-1).") grupa_pr_temp(".implode(',',$columns).")";
		DB::statement("INSERT INTO grupa_pr (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM grupa_pr t WHERE t.grupa=grupa_pr_temp.grupa))");


		$proizvodjac_rows="";
		foreach (self::uniqueManufacturers($products) as $key => $manufacturerName) {
		$proizvodjac_rows .= "(nextval('proizvodjac_proizvodjac_id_seq'),'".$manufacturerName."',NULL,0,0,0,NULL,NULL,NULL,1,(NULL)::integer,(NULL)::integer,'".$manufacturerName."',0,NULL,NULL),";
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$table_temp = "(VALUES ".substr($proizvodjac_rows,0,-1).") proizvodjac_temp(".implode(',',$columns).")";		
		DB::statement("INSERT INTO proizvodjac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM proizvodjac t WHERE t.naziv=proizvodjac_temp.naziv))");


		$getMappedGroups = self::getMappedGroups();
		$features = self::features($products);
		$feature_names_rows ="";
		foreach($features as $group => $featureNames){
			$grupaPrId = $getMappedGroups[$group];

			$rbrNaziv = 0;
			foreach($featureNames as $featureName => $values){
				$rbrNaziv++;

				$feature_names_rows .= "(".strval($grupaPrId).",1,'".$featureName."',".$rbrNaziv."),";
			}
		}
		$table_temp = "(VALUES ".substr($feature_names_rows,0,-1).") grupa_pr_naziv_temp(grupa_pr_id,active,naziv,rbr)";
		DB::statement("INSERT INTO grupa_pr_naziv (grupa_pr_id,active,naziv,rbr) SELECT * FROM ".$table_temp."");


		$mappedFeatureNames = self::getMappedFeatureNames();
		$feature_name_values_rows ="";
		foreach($features as $group => $featureNames){
			foreach($featureNames as $featureName => $values){
				$grupaPrNazivId = $mappedFeatureNames[$group.'<=>'.$featureName];
				$rbrVrednost = 0;
				foreach($values as $value){
					$rbrVrednost++;
					$feature_name_values_rows .= "(".strval($grupaPrNazivId).",1,'".$value."',".$rbrVrednost."),";
				}
			}
		}
		$table_temp = "(VALUES ".substr($feature_name_values_rows,0,-1).") grupa_pr_vrednost_temp(grupa_pr_naziv_id,active,naziv,rbr)";
		DB::statement("INSERT INTO grupa_pr_vrednost (grupa_pr_naziv_id,active,naziv,rbr) SELECT * FROM ".$table_temp."");


		$getMappedManufacturers = self::getMappedManufacturers();

		$article_rows="";
		foreach ($products as $article) {
			$extId = $article->IDArtikal;
			$sifra = $article->sifra_artikla;
			$naziv = self::convert($article->naziv);
			$grupaId = (isset($getMappedGroups[strval($article->grupa)]) ? $getMappedGroups[strval($article->grupa)] : -1);
			$proizvodjacId = (isset($getMappedManufacturers[strval($article->proizvodjac)]) ? strval($getMappedManufacturers[strval($article->proizvodjac)]) :' -1');
			$nc = isset($article->cena_nc) ? $article->cena_nc : 0.00;
			$mc = isset($article->web_cena) ? $article->web_cena : 0.00;
			$webCena = isset($article->web_cena) ? $article->web_cena : 0.00;
			$opis = isset($article->opis) ? "'".$article->opis."'" : "NULL";
			$tezina = isset($article->{'težina'}) ? strval(floatval($article->{'težina'})*1000) : "(NULL)::numeric";
			$karakteristike = isset($article->karakteristike_list) ? "'".strval($article->karakteristike_list)."'" : "NULL";
			$flagKarakteristike = 0;


			$article_rows .= "(nextval('roba_roba_id_seq'),NULL,'".$naziv."',NULL,NULL,NULL,".strval($grupaId).",0,1,".$proizvodjacId.",-1,nextval('roba_roba_id_seq'),NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,".$tezina.",9,0,0,0,0,1,1,0,NULL,1,".strval($nc).",0,".strval($nc).",0,NULL,".strval($mc).",false,0,(NULL)::integer,'".substr($naziv,0,199)."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($webCena).",1,0,".$opis.",".$karakteristike.",NULL,".strval($flagKarakteristike).",0,0.00,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra)."','".strval($extId)."',0,(NULL)::integer,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL),";
		

		}

		$getMappedArticles = self::getMappedArticles();
		$mappedFeatureValues = self::getMappedFeatureValues();

		$article_feature_values_rows ="";
		foreach ($products as $article) {
			if(isset($getMappedArticles[strval($article->IDArtikal)])){
				$robaId = $getMappedArticles[strval($article->IDArtikal)];

				foreach($article->xpath('karakteristike/karakteristika') as $karakteristika){
					if(!empty($naziv = $karakteristika->attributes()->ime) && !empty($vrednost = substr(trim(strval($karakteristika->vrednost)),0,255)) && isset($mappedFeatureNames[strval($article->grupa).'<=>'.trim($naziv)]) && isset($mappedFeatureValues[strval($article->grupa).'<=>'.trim($naziv).'<=>'.$vrednost])){

						$article_feature_values_rows .= "(".$robaId.",".$mappedFeatureNames[strval($article->grupa).'<=>'.trim($naziv)].",'".$vrednost."',".$mappedFeatureValues[strval($article->grupa).'<=>'.trim($naziv).'<=>'.$vrednost]."),";
					}
				}
			}
			
		}

		$table_temp = "(VALUES ".substr($article_feature_values_rows,0,-1).") web_roba_karakteristike_temp(roba_id,grupa_pr_naziv_id,vrednost,grupa_pr_vrednost_id)";
		DB::statement("INSERT INTO web_roba_karakteristike (roba_id,grupa_pr_naziv_id,vrednost,grupa_pr_vrednost_id) SELECT * FROM ".$table_temp."");




		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".substr($article_rows,0,-1).") roba_temp(".implode(',',$columns).")";
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.naziv=roba_temp.naziv))");

		DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž')");
		DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š')");
		DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ')");
		DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č')");
		DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć')");
		DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž')");
		DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š')");
		DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ')");
		DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č')");
		DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć')");
		DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž')");


		foreach ($products as $product) {
			if(isset($getMappedArticles[strval($product->IDArtikal)])){
				foreach(range(0,10) as $key){
					if(isset($product->slike->slika[$key])){
						$robaId = $getMappedArticles[strval($product->IDArtikal)];
						$imageLink = $product->slike->slika[$key];

				    	$extension = self::fileExtension($imageLink);
				    	if(in_array($extension,array('jpg','png','jpeg','JPG','PNG','JPEG'))){
							try { 
			                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
			                    $name = $slika_id.'.jpg';
			                    $destination = 'images/products/big/'.$name;
			                    $akcija = $key == 0 ? 1 : 0;
	                    	
		                        $content = @file_get_contents($imageLink);
		                        file_put_contents($destination,$content);

			                    $insert_data = array(
			                        'web_slika_id'=>intval($slika_id),
			                        'roba_id'=> $robaId,
			                        'akcija'=>$akcija, 
			                        'flag_prikazi'=>1,
			                        'putanja'=>'images/products/big/'.$name
			                        );

			                    DB::table('web_slika')->insert($insert_data);
							}
							catch (Exception $e) {
							}
						}

					}
				}
			}
		}





		$this->info('Finished.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
